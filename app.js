//Tutorial: http://cwbuecheler.com/web/tutorials/2013
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var h5bp = require('h5bp');
var routes = require('./routes/index');
var users = require('./routes/users');
var Client = require('node-rest-client').Client;
var childProcess = require('child_process');
//var phantomjs = require('phantomjs-prebuilt');
var Horseman = require('node-horseman');
//var binPath = phantomjs.path;
var PORT = process.env.PORT || 3000;
//Documentation: https://www.npmjs.com/package/node-rest-client
var pkgcloud = require('pkgcloud'),
    _ = require('lodash');
var bibtexParse = require('bibtex-parse-js');
var scholar = require('google-scholar-link');
var OAuth = require('oauth').OAuth;
//END require statements

//initialize app
var app = express();
var http = require('http').createServer(app);
http.listen(PORT, function () {
    console.log('listening on *:' + PORT);
});
// view engine setup
app.set('public', path.join(__dirname, 'public'));  //setting the static folder to public
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(h5bp({
    root: __dirname + '/public'
}));
//use --save at the end of the npm install to add the lib to json dependencies
app.use(express.static(path.join(__dirname, 'public')));

//Scrape OpenStack
var horseman = new Horseman();

function yyyymmdd(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
//var date = new Date();
//date.yyyymmdd();

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};
//Date.addDays(5);

Date.daysBetween = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
};

function getTable() {
    return horseman.evaluate(function () {
//            // This code is executed in the browser.
        console.log('yyyy');
        function yyyymmdd(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        var selector = "#global_usage tbody tr";
        var rows = [];
        console.log('currentdate');
        var currentDateYY = yyyymmdd($('#id_start').val());
        console.log('after current date');
        $(selector).each(function (index, item) {      //loop through each project/row
            var name, vcpus, disk, ram;
            console.log(item);
            $($(item).children()).each(function (index, td) {    //loop through each td item
                var value = $(td).html();
                console.log('val');
                switch (index) {
                    case 0:
                        name = value.trim();
                        break;
                    case 1:
                        vcpus = parseFloat(value.trim().replace(/\D/g, ''));
                        break;
                    case 2:
                        disk = parseFloat(value.trim().replace(/\D/g, ''));
                        break;
                    case 3:
                        ram = parseFloat(value.trim().replace(/\D/g, ''));
                        break;
                }
            });
            var rowArray = [name, currentDateYY.substring(5), ram, disk, vcpus];
            console.log('9');
            rows.push(rowArray);
        });
        console.log('11');
        return rows;
    })
}
//if (masterProduct[name] == null){
//    masterProduct[name] = [['Date', 'Ram Usage (GB)', 'Disk', 'VCPUs'], rowArray];
//}
//else{
//    masterProduct[name].push(rowArray)
//}
app.get('/getProjectHistory', function (req, res) {
    res.send(masterProduct);
});
function hasNextPage() {
    console.log('has next page');
    startDate = startDate.addDays(3);  //TODO: change this to a lower number when you push to the server
    var today = new Date();
    if (startDate > today) {
        return horseman.exists("#adfasdfs");    //if you're up to a date that's past today, end it. Siple way of sending back a promise with false
    }
    return horseman
        .value('#id_start', yyyymmdd(startDate))
        .value('#id_end', yyyymmdd(startDate))
        .click('#date_form button')
        .waitForNextPage()
        .exists('#id_end');
}

var masterProduct = {};
function scrape() {
    return new Promise(function (resolve, reject) {
        return getTable()
            .then(function (newRows) {
                for (var i = 0; i < newRows.length; i++){
                    var rowArray = newRows[i];
                    console.log('new rows');
                    console.log(newRows);
                    var name = rowArray[0];
                    rowArray.splice(0,1);
                    if (masterProduct[name] == null){
                        masterProduct[name] = [['Date', 'Ram Usage (GB)', 'Disk', 'VCPUs'], rowArray];
                    }
                    else{
                        masterProduct[name].push(rowArray);
                    }
                }
                return hasNextPage()
                    .then(function (hasNext) {
                        if (hasNext) {
                            return horseman
                                .wait(1)
                                .then(scrape);
                        }
                    });
            })
            .then(resolve);
    });
}

var startDate = new Date('2016', '03', '01');
//console.log('horseman start');
//horseman
//    .userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
//    .log('before open')
//    .open('https://cloud.compas.cs.stonybrook.edu/horizon/admin/')
//    .log('after open')
//    .waitForSelector('#loginBtn')   //wait for the login button to show
//    .log('after loginbtn')
//    .click('#loginBtn') //click "login" on openstack since the "use gmail" option is already selected
//    .log('after click')
//    .waitForNextPage()
//    .log('before email enter')
//    .type('#Email', 'arambag@cs.stonybrook.edu')    //enter my email
//    .keyboardEvent('keypress', 16777221)  //click on the "next field" button to show the password field
//    .wait(1000) //wait for it show up
//    .type('#Passwd', 'myGooglePassword')  //enter my password
//    .log('before signinclick')
//    .click('#signIn')
//    .log('after signinclick')
//    .waitForNextPage()
//    .log('in between')
//    .waitForSelector('#id_start')    //wait for the main page that we know and love to load
//    .log('15')
//    .value('#id_start', yyyymmdd(startDate))
//    .value('#id_end', yyyymmdd(startDate))
//    .log('15.5')
//    .wait(2000)
//    .click('#date_form button')
//    .wait(5000)
//    .log('16')
//    .wait(5000)  //failed to load url here
//    .log('17')
//    .then(scrape)
//    .log('done')
//    .close();
//End Scrape OpenStack

var api_key = 'key-9f25ba4ad1200d45612172f4ac993a65';
var domain = 'sandbox0bfb41f9eecc4bc18fece256ac5bd470.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

app.post('/sendServerStatus', function (req, res) {
    try {
        var data = {
            from: 'OpenStack COMPAS Status Page <COMPASStatus@smtp.mailgun.org>',
            to: 'abagh0703@gmail.com',
            subject: 'OpenStack Server down',
            text: 'The network "' + req.name + '" belonging to the project "' + req.projName + '" went from ACTIVE to ' + req.status
        };
        mailgun.messages().send(data, function (error, body) {
            console.log(body);
        });
    }
    catch(err){}
});

app.post('/sendGaugeStatus', function (req, res) {
    var data = {
        from: 'OpenStack COMPAS Status Page <COMPASStatus@smtp.mailgun.org>',
        to: 'abagh0703@gmail.com',
        subject: 'OpenStack Nearing Limit',
        text: 'Your OpenStack cloud is currently using ' + req.currentUsage + ' ' + req.name + ' out of the maximum ' + req.maxUsage + ' (' + Math.round(req.currentUsage / req.maxUsage) + ')'
    };
    mailgun.messages().send(data, function (error, body) {
        console.log(body);
    });
});


//START PUBLICATIONS CODE
//var horseman2 = new Horseman();

//function paperActions(){
//    console.log('5');
//    return horseman2.evaluate(function(){
//        // This code is executed in the browser.
//        console.log('inside');
//        var selector = '#gs_ggsW0';
//            var returnVal = {
//                link : $( selector ).attr('href'),
//                count: count + 1
//            };
//        return returnVal;
//    });
//}

//function hasNextValue(){
//    return horseman2.exists('body');
//}

//function scrapePaper(){
//    return new Promise( function( resolve, reject ){
//        console.log('1');
//        return paperActions()
//            .then(function(newLinks){
//                console.log('2');
//                storeLinks.push(newLinks.link);
//                if ( newLinks.count != rawPubs.length ){
//                    return hasNextValue()
//                        .then(function(hasNext){
//                            if (hasNext){
//                                return horseman2
//                                    .open(pubsArray[newLinks.count].link)
//                                    .waitForNextPage()
//                                    .then( scrapePaer );
//                            }
//                        });
//                }
//            })
//            .then( resolve );
//    });
//}

app.get('/getPublications', function(req, res) {
    try {
        var rawPub;
        var rawPubs = [];
        var pubsArray = [];
        var pub;
        rawPub = bibtexParse.toJSON('@inproceedings{sze2016hardening,' +
            'title={Hardening OpenStack Cloud Platforms against Compute Node Compromises},' +
            'author={Sze, Wai Kit and Srivastava, Abhinav and Sekar, R},' +
            'booktitle={Proceedings of the 11th ACM on Asia Conference on Computer and Communications Security},' +
            'pages={341--352},' +
            'year={2016},' +
            'organization={ACM}' +
            '}')[0];
        //rawPub.entryTags.abstract = 'saf';
        rawPub.entryTags.abstract = 'Infrastructure-as-a-Service (IaaS) clouds such as OpenStack consist of two kinds of nodes in their infrastructure: control nodes and compute nodes. While control nodes run all critical services, compute nodes host virtual machines of customers. Given the large number of compute nodes, and the fact that they are hosting VMs of (possibly malicious) customers, it is possible that some of the compute nodes may be compromised. This paper examines the impact of such a compromise. We focus on OpenStack, a popular open-source cloud platform that is widely adopted. We show that attackers compromising a single compute node can extend their controls over the entire cloud infrastructure. They can then gain free access to resources that they have not paid for, or even bring down the whole cloud to affect all customers. This startling result stems from the cloud platform’s misplaced trust, which does not match today’s threats. To overcome the weakness, we propose a new system, called SOS , for hardening OpenStack. SOS limits trust on compute nodes. SOS consists of a framework that can enforce a wide range of security policies. Specifically, we applied mandatory access control and capabilities to con- fine interactions among different components. Effective con- finement policies are generated automatically. Furthermore, SOS requires no modifications to the OpenStack. This has allowed us to deploy SOS on multiple versions of OpenStack. Our experimental results demonstrate that SOS is scalable, incurs negligible overheads and offers strong protection.';
        rawPubs.push(rawPub);

        rawPub = bibtexParse.toJSON('@inproceedings{baig2014cloudflow,' +
            'title={CloudFlow: Cloud-wide policy enforcement using fast VM introspection},' +
            'author={Baig, Mirza Basim and Fitzsimons, Connor and Balasubramanian, Suryanarayanan and Sion, Radu and Porter, Donald E},' +
            'booktitle={Cloud Engineering (IC2E), 2014 IEEE International Conference on},' +
            'pages={159--164},' +
            'year={2014},' +
            'organization={IEEE}' +
            '}')[0];
        rawPub.entryTags.abstract = 'Government and commercial enterprises are increasingly considering cloud adoption. Clouds improve overall efficiency by consolidating a number of different clients’ software virtual machines onto a smaller set of hardware resources. Unfortunately, this shared hardware also creates inherent side-channel vulnerabilities, which an attacker can use to leak information from a victim VM. Side-channel vulnerabilities are especially concerning when different principals are constrained by regulations. A classic example of these regulations are Chinese Wall policies for financial companies, which aim to protect the financial system from illicit manipulation by separating portions of the business with conflicting interests. Although efficient prevention of side channels is difficult within a single node, there is a unique opportunity within a cloud. This paper proposes a low-overhead approach to cloudwide information flow policy enforcement: identifying side channels which could potentially be used to violate a security policy through run-time introspection, and reactively migrating virtual machines to eliminate node-level side-channels. In this paper we describe CloudFlow—an information flow control extension for OpenStack. CloudFlow includes a novel, virtual machine introspection mechanism that is orders of magnitude faster than previous approaches. CloudFlow efficiently and transparently enforces information flow policies cloudwide, including information leaks through undesirable sidechannels. Additionally, CloudFlow has potential uses for cloud management and resource-efficient virtual machine scheduling.';
        rawPubs.push(rawPub);

        rawPub = bibtexParse.toJSON('@inproceedings{gandhi2015model, title={Model-driven Autoscaling for Hadoop clusters}, author={Gandhi, Anshul and Dube, Parijat and Kochut, Andrzej and Zhang, Li}, booktitle={Autonomic Computing (ICAC), 2015 IEEE International Conference on}, pages={155--156}, year={2015}, organization={IEEE} }')[0];
        rawPub.entryTags.abstract = 'In this paper, we present the design and implementation of a model-driven auto scaling solution for Hadoop clusters. We first develop novel performance models for Hadoop workloads that relate job completion times to various workload and system parameters such as input size and resource allocation. We then employ statistical techniques to tune the models for specific workloads, including Terasort and K-means. Finally, we employ the tuned models to determine the resources required to successfully complete the Hadoop jobs as per the user-specified response time SLA. We implement our solution on an Open Stack-based cloud cluster running Hadoop. Our experimental results across different workloads demonstrate the auto scaling capabilities of our solution, and enable significant resource savings without compromising performance.';
        rawPubs.push(rawPub);

        rawPub = bibtexParse.toJSON('@inproceedings{gandhi2015model, title={Model-driven Autoscaling for Hadoop clusters}, author={Gandhi, Anshul and Dube, Parijat and Kochut, Andrzej and Zhang, Li}, booktitle={Autonomic Computing (ICAC), 2015 IEEE International Conference on}, pages={155--156}, year={2015}, organization={IEEE} }')[0];
        rawPub.entryTags.abstract = "In this paper, we present the design and implementation of a model-driven auto scaling solution for Hadoop clusters. We first develop novel performance models for Hadoop workloads that relate job completion times to various workload and system parameters such as input size and resource allocation. We then employ statistical techniques to tune the models for specific workloads, including Terasort and K-means. Finally, we employ the tuned models to determine the resources required to successfully complete the Hadoop jobs as per the user-specified response time SLA. We implement our solution on an Open Stack-based cloud cluster running Hadoop. Our experimental results across different workloads demonstrate the auto scaling capabilities of our solution, and enable significant resource savings without compromising performance.";
        rawPubs.push(rawPub);

        rawPub = bibtexParse.toJSON('@article{pentikousis2015guest, title={Guest editorial: Network and service virtualization}, author={Pentikousis, Kostas and Meirosu, Catalin and Lopez, Diego R and Denazis, Spyros and Shiomoto, Kohei and Westphal, Fritz-Joachim}, journal={IEEE Communications Magazine}, volume={53}, number={2}, pages={88--89}, year={2015}, publisher={IEEE} }')[0];
        rawPub.entryTags.abstract = "The introduction of software-defined networking (SDN) and network function virtualization (NFV) has altered, in a wholesale manner, the way to plan for network infrastructure evolution in the forthcoming decade. This Feature Topic aims to provide a concise reference entry point to a wider audience with respect to carrier-grade networking and service virtualization with emphasis on automating the entire networking and cloud infrastructure. The first steps in network virtualization are already taking place today, although more often than not through a piecemeal approach that simply replaces hardware-based network appliances with software-based alternatives. This may help to reduce operator costs in the mid-term but does not alter the full life cycle of service creation and deployment. The real potential for network and service virtualization lies in upgrading the entire toolbox network operators have at their disposal, as state-of-the-art research and development efforts already indicate. For example, the European FP7 UNIFY project defines an architecture where the entire network, from home devices to data centers, forms a unified production environment, a dynamic service creation platform able to distribute functions and state anywhere in the network, aided by automated orchestration engines; see www.fp7-unify.eu for more details. While drafting the Call for Papers for this Feature Topic, our first goal was to attract high-quality contributions from operator and industry research labs as this topic is particularly pertinent to practitioners in the field. Carriers, in particular, can be the main beneficiaries from the emerging infrastructures based on NFV, SDN, and cloud technologies. Therefore, articles by authors working at global operators currently developing, evaluating, and standardizing solutions for network and service virtualization were particularly welcome and encouraged. In this sense, we are glad that all five selected papers for publication in this issu- are penned by experts affiliated with European, American, and Asian carriers.. We first develop novel performance models for Hadoop workloads that relate job completion times to various workload and system parameters such as input size and resource allocation. We then employ statistical techniques to tune the models for specific workloads, including Terasort and K-means. Finally, we employ the tuned models to determine the resources required to successfully complete the Hadoop jobs as per the user-specified response time SLA. We implement our solution on an Open Stack-based cloud cluster running Hadoop. Our experimental results across different workloads demonstrate the auto scaling capabilities of our solution, and enable significant resource savings without compromising performance.";
        rawPubs.push(rawPub);

        rawPub = bibtexParse.toJSON('@inproceedings{gandhi2014modeling, title={Modeling the impact of workload on cloud resource scaling}, author={Gandhi, Anshul and Dube, Parijat and Karve, Alexei and Kochut, Andrzej and Zhang, Li}, booktitle={Computer Architecture and High Performance Computing (SBAC-PAD), 2014 IEEE 26th International Symposium on}, pages={310--317}, year={2014}, organization={IEEE} }')[0];
        rawPub.entryTags.abstract = "Cloud computing offers the flexibility to dynamically size the infrastructure in response to changes in workload demand. While both horizontal and vertical scaling of infrastructure is supported by major cloud providers, these scaling options differ significantly in terms of their cost, provisioning time, and their impact on workload performance. Importantly, the efficacy of horizontal and vertical scaling critically depends on the workload characteristics, such as the workload's parallelizability and its core scalability. In today's cloud systems, the scaling decision is left to the users, requiring them to fully understand the tradeoffs associated with the different scaling options. In this paper, we present our solution for optimizing the resource scaling of cloud deployments via implementation in OpenStack. The key component of our solution is the modelling engine that characterizes the workload and then quantitatively evaluates different scaling options for that workload. Our modelling engine leverages Amdahl's Law to model service time scaling in scaleup environments and queueing-theoretic concepts to model performance scaling in scale-out environments. We further employ Kalman filtering to account for inaccuracies in the model-based methodology, and to dynamically track changes in the workload and cloud environment.. This Feature Topic aims to provide a concise reference entry point to a wider audience with respect to carrier-grade networking and service virtualization with emphasis on automating the entire networking and cloud infrastructure. The first steps in network virtualization are already taking place today, although more often than not through a piecemeal approach that simply replaces hardware-based network appliances with software-based alternatives. This may help to reduce operator costs in the mid-term but does not alter the full life cycle of service creation and deployment. The real potential for network and service virtualization lies in upgrading the entire toolbox network operators have at their disposal, as state-of-the-art research and development efforts already indicate. For example, the European FP7 UNIFY project defines an architecture where the entire network, from home devices to data centers, forms a unified production environment, a dynamic service creation platform able to distribute functions and state anywhere in the network, aided by automated orchestration engines; see www.fp7-unify.eu for more details. While drafting the Call for Papers for this Feature Topic, our first goal was to attract high-quality contributions from operator and industry research labs as this topic is particularly pertinent to practitioners in the field. Carriers, in particular, can be the main beneficiaries from the emerging infrastructures based on NFV, SDN, and cloud technologies. Therefore, articles by authors working at global operators currently developing, evaluating, and standardizing solutions for network and service virtualization were particularly welcome and encouraged. In this sense, we are glad that all five selected papers for publication in this issu- are penned by experts affiliated with European, American, and Asian carriers.. We first develop novel performance models for Hadoop workloads that relate job completion times to various workload and system parameters such as input size and resource allocation. We then employ statistical techniques to tune the models for specific workloads, including Terasort and K-means. Finally, we employ the tuned models to determine the resources required to successfully complete the Hadoop jobs as per the user-specified response time SLA. We implement our solution on an Open Stack-based cloud cluster running Hadoop. Our experimental results across different workloads demonstrate the auto scaling capabilities of our solution, and enable significant resource savings without compromising performance.";
        rawPubs.push(rawPub);

        rawPub = bibtexParse.toJSON('@inproceedings{michalewicz2016infinicortex, title={InfiniCortex: present and future invited paper}, author={Michalewicz, Marek T and Lian, Tan Geok and Seng, Lim and Low, Jonathan and Southwell, David and Gunthorpe, Jason and Noaje, Gabriel and Chien, Dominic and Poppe, Yves and Chrzeszczyk, Jakub and others}, booktitle={Proceedings of the ACM International Conference on Computing Frontiers}, pages={267--273}, year={2016}, organization={ACM} }')[0];
        rawPub.entryTags.abstract = "Commencing in June 2014, A*STAR Computational Resource Centre (A*CRC) team in Singapore, together with dozens of partners world-wide, have been building the In- finiCortex. Four concepts are integrated together to realise InfiniCortex: i) High bandwidth (∼ 10 to 100Gbps) intercontinental connectivity between four continents: Asia, North America, Australia and Europe; ii) InfiniBand extension technology supporting transcontinental distances using Obsidian’s Longbow range extenders; iii) Connecting separate InfiniBand sub-nets with different net topologies to create a single computational resource: Galaxy of Supercomputers [10] iv) Running workflows and applications on such a distributed computational infrastructure. We have successfully demonstrated InfiniCortex prototypes at SC14 and SC15 conferences. The infrastructure comprised of computing resources residing at multiple locations in Singapore, Japan, Australia, USA, Canada, France and Poland. Various concurrent applications, including work- flows, I/O heavy applications enabled with ADIOS system, Extempore real-time interactive applications, and in-situ realtime visualisations were demonstrated. In this paper we briefly report on basic ideas behind InfiniCortex construct, our recent successes and some ideas about further growth and extension of this project.. While both horizontal and vertical scaling of infrastructure is supported by major cloud providers, these scaling options differ significantly in terms of their cost, provisioning time, and their impact on workload performance. Importantly, the efficacy of horizontal and vertical scaling critically depends on the workload characteristics, such as the workload's parallelizability and its core scalability. In today's cloud systems, the scaling decision is left to the users, requiring them to fully understand the tradeoffs associated with the different scaling options. In this paper, we present our solution for optimizing the resource scaling of cloud deployments via implementation in OpenStack. The key component of our solution is the modelling engine that characterizes the workload and then quantitatively evaluates different scaling options for that workload. Our modelling engine leverages Amdahl's Law to model service time scaling in scaleup environments and queueing-theoretic concepts to model performance scaling in scale-out environments. We further employ Kalman filtering to account for inaccuracies in the model-based methodology, and to dynamically track changes in the workload and cloud environment.. This Feature Topic aims to provide a concise reference entry point to a wider audience with respect to carrier-grade networking and service virtualization with emphasis on automating the entire networking and cloud infrastructure. The first steps in network virtualization are already taking place today, although more often than not through a piecemeal approach that simply replaces hardware-based network appliances with software-based alternatives. This may help to reduce operator costs in the mid-term but does not alter the full life cycle of service creation and deployment. The real potential for network and service virtualization lies in upgrading the entire toolbox network operators have at their disposal, as state-of-the-art research and development efforts already indicate. For example, the European FP7 UNIFY project defines an architecture where the entire network, from home devices to data centers, forms a unified production environment, a dynamic service creation platform able to distribute functions and state anywhere in the network, aided by automated orchestration engines; see www.fp7-unify.eu for more details. While drafting the Call for Papers for this Feature Topic, our first goal was to attract high-quality contributions from operator and industry research labs as this topic is particularly pertinent to practitioners in the field. Carriers, in particular, can be the main beneficiaries from the emerging infrastructures based on NFV, SDN, and cloud technologies. Therefore, articles by authors working at global operators currently developing, evaluating, and standardizing solutions for network and service virtualization were particularly welcome and encouraged. In this sense, we are glad that all five selected papers for publication in this issu- are penned by experts affiliated with European, American, and Asian carriers.. We first develop novel performance models for Hadoop workloads that relate job completion times to various workload and system parameters such as input size and resource allocation. We then employ statistical techniques to tune the models for specific workloads, including Terasort and K-means. Finally, we employ the tuned models to determine the resources required to successfully complete the Hadoop jobs as per the user-specified response time SLA. We implement our solution on an Open Stack-based cloud cluster running Hadoop. Our experimental results across different workloads demonstrate the auto scaling capabilities of our solution, and enable significant resource savings without compromising performance.";
        rawPubs.push(rawPub);

        for (var i = 0; i < rawPubs.length; i++) {
            pub = rawPubs[i];     //rawPubs is an array of paper objects
            //pub = pub;   //Bibtex returns an object in an array
            pub.entryTags.link = scholar(pub.entryTags.title);
            pubsArray.push(pub);
            //horseman2
            //    .userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
            //    .log('scholar before open')
            //    .open(pub.entryTags.link)
            //    .log('after open')
            //    .waitForSelector('#gs_ggsW0')   //wait for the login button to show
            //    .log('after loginbtn')
            //    .then(scrapePaper)
            //    .log('done2')
            //    .close()
        }
        res.send(pubsArray);
    }
    catch(err){
        res.send('');
    }
});
//END PUBLICATIONS CODE

//setup OpenStack nodejs pkgcloud
var openstackCompute = pkgcloud.compute.createClient({
    provider: 'openstack', // required
    username: 'arambag@cs.stonybrook.edu', // required
    password: 'openstackk111', // required
    authUrl: 'https://cloud.compas.cs.stonybrook.edu:5000/', // required,
    region: 'RegionOne'
});

var openstackNetwork = pkgcloud.network.createClient({
    provider: 'openstack', // required
    username: 'arambag@cs.stonybrook.edu', // required
    password: 'openstackk111', // required
    authUrl: 'https://cloud.compas.cs.stonybrook.edu:5000/', // required,
    region: 'RegionOne'
});

var openstackOrchestration = pkgcloud.orchestration.createClient({
    provider: 'openstack', // required
    username: 'arambag@cs.stonybrook.edu', // required
    password: 'openstackk111', // required
    authUrl: 'https://cloud.compas.cs.stonybrook.edu:5000/', // required,
    region: 'RegionOne'
});

app.get('/getOpenServers', function (req, res) {
    openstackCompute.getFlavors(function (err, servers) {
        res.send(servers);
    })
});

function randomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

//BEGIN Contact MAAS
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
app.get('/getMaasNodes', function(req, res) {
    try {
        require('oauth-zero-legged');
        var client2 = require('oauth-zero-legged/lib/Client');
        var currentUnix = Math.floor((new Date).getTime() / 1000);
        var maasNonce = randomString(6);
        var options = {
            "method": "GET",
            "url": "https://maas.compas.cs.stonybrook.edu/MAAS/api/1.0/nodes/?op=list&oauth_consumer_key=JZMNWXbVJvfjX7KJcB&oauth_token=X2wetzMZGVaEdAReYw&oauth_signature_method=PLAINTEXT&oauth_timestamp=" + currentUnix + "&oauth_nonce=" + maasNonce + "&oauth_version=1.0&oauth_signature=%26LmUAbwMxhSJAtdfFWrS8xksw2Bq8eW8Z",
            "headers": {
                "Content-Type": "application/json"
            }
        };
// with oauth
        var my_success_callback = function (response) {
            var responseStatusCode = response.statusCode;
            var responseHeaders = response.headers;
            var responseHttpVersion = response.httpVersion;
            var responseBody = response.body;
            res.send(responseBody)
        };
        var my_failure_callback = function (response) {
            var responseCode = response.code;
            var responseErrno = response.errno;
            var responseSyscall = response.syscall;
            console.log('error: ' + responseErrno);
            console.log(responseCode);
            console.log(responseSyscall);
        };
        client2.request(options, my_success_callback, my_failure_callback, "compassys", "c0^^p@$$");
    }
    catch(err){
        res.send('');
    }
});
//END Contact MAAS

//Make external requests to OpenStack's REST API
var client = new Client();
var xAuthToken = '';
var tokenArgs = {
    data: {
        "auth": {
            "tenantId": "857a0f345123417c9f49d60847370476",  //TODO: change requests when you start the server
            "passwordCredentials": {
                "username": "arambag@cs.stonybrook.edu",
                "password": "openstackk111"
            }
        }
    },
    headers: {"Content-Type": "application/json"}
};

var args = {
    headers: {"X-Auth-Token": xAuthToken} // request headers
};
var osIP = '130.245.168.7'; //IP os os-controller. from hosts file at c:\windows\system32\drivers\etc\hosts

var getProjectsCurrentDate = '';
var getProjectsCreatedDate = '';
app.get('/getProjects', function (req, res) {
    console.log('ran');
    //Gets the X-Auth-Token
    if (getProjectsCurrentDate == '' || Date.daysBetween(getProjectsCurrentDate, getProjectsCreatedDate) >= 5) {  //if it's the first time or if it's been 5 days
        try {
            client.post('https://cloud.compas.cs.stonybrook.edu:5000/v2.0/tokens', tokenArgs, function (data, response) {
                if (data.access == null){
                    console.log('get projects null');
                    res.send('');
                    return;
                }
                xAuthToken = data.access.token.id;
                args = {
                    headers: {"X-Auth-Token": xAuthToken} //f933d00c7a2d43e3bb5657176aa44655
                };
                client.get("https://cloud.compas.cs.stonybrook.edu:5000/v3/projects", args, function (data, response) {
                    getProjectsCreatedDate = new Date();
                    getProjectsCurrentDate = getProjectsCreatedDate;
                    // parsed response body as js object
                    res.send(data);
                });
            });
        }
        catch(err){
            res.send('');
        }
    }
    else {
        console.log('else');
        try {
            client.get("https://cloud.compas.cs.stonybrook.edu:5000/v3/projects", args, function (data, response) {
                // parsed response body as js object
                res.send(data);
            });
        }
        catch(err){
            res.send('');
        }
    }
});

//console.log('testing rest');
//client.get('http://maas.compas.cs.stonybrook.edu/MAAS/api/1.0/nodes/?op=list&oauth_consumer_key=JZMNWXbVJvfjX7KJcB&oauth_token=X2wetzMZGVaEdAReYw&oauth_signature_method=PLAINTEXT&oauth_timestamp=1469559977&oauth_nonce=6sIFPk&oauth_version=1.0&oauth_signature=%26LmUAbwMxhSJAtdfFWrS8xksw2Bq8eW8Z', function(data, response){
//    console.log('success!');
//    console.log(data.toString('utf8'));
//});

var getNetworksCurrentDate = '';
var getNetworksCreatedDate = '';
app.get('/getNetworks', function (req, res) {
    console.log('get networks');
    if (getNetworksCurrentDate == '' || Date.daysBetween(getNetworksCurrentDate, getNetworksCreatedDate) >= 5) {
        try {
            client.post('https://cloud.compas.cs.stonybrook.edu:5000/v2.0/tokens', tokenArgs, function (data, response) {
                if (data.access == null){
                    console.log('get networks null');
                    res.send('');
                    return;
                }
                xAuthToken = data.access.token.id;
                args = {
                    headers: {"X-Auth-Token": xAuthToken}
                };
                client.get("http://" + osIP + ":9696/v2.0/networks?fields=id&fields=name&fields=status&fields=tenant_id", args, function (data, response) {
                    getNetworksCreatedDate = new Date();
                    getNetworksCurrentDate = getNetworksCreatedDate;
                    // parsed response body as js object
                    res.send(data);
                });
            });
        }
        catch(err){
            res.send('');
        }
    }
    else {
        try {
            client.get("http://" + osIP + ":9696/v2.0/networks?fields=id&fields=name&fields=status&fields=tenant_id", args, function (data, response) {
                // parsed response body as js object
                res.send(data);
            });
        }
        catch(err){
            res.send('');
        }
    }
});

var getLdCurrentDate = '';
var getLdCreatedDate = '';
app.get('/getLocalDisk', function (req, res) {
    if (getLdCurrentDate == '' || Date.daysBetween(getLdCurrentDate, getLdCreatedDate) >= 5) {
        try {
            client.post('https://cloud.compas.cs.stonybrook.edu:5000/v2.0/tokens', tokenArgs, function (data, response) {
                if (data.access == null){
                    console.log('null local disk');
                    res.send('');
                    return;
                }
                xAuthToken = data.access.token.id;
                args = {
                    headers: {"X-Auth-Token": xAuthToken}
                };
                client.get("http://" + osIP + ":8774/v2/857a0f345123417c9f49d60847370476/os-hypervisors/statistics", args, function (data, response) {
                    getLdCreatedDate = new Date();
                    getLdCurrentDate = getLdCreatedDate;
                    res.send(data);
                });
            });
        }
        catch(err){
            res.send('');
        }
    }
    else {
        try {
            client.get("http://" + osIP + ":8774/v2/857a0f345123417c9f49d60847370476/os-hypervisors/statistics", args, function (data, response) {
                res.send(data);
            });
        }
        catch(err){
            res.send('');
        }
    }
});

var getPqCurrentDate = '';
var getPqCreatedDate = '';
app.post('/getProjectQuotas', function (req, res) {
    if (getPqCurrentDate == '' || Date.daysBetween(getPqCurrentDate, getPqCreatedDate) >= 5) {
        try {
            client.post('https://cloud.compas.cs.stonybrook.edu:5000/v2.0/tokens', tokenArgs, function (data, response) {
                if (data.access == null){
                    console.log('null');
                    res.send('');
                    return;
                }
                xAuthToken = data.access.token.id;
                args = {
                    headers: {"X-Auth-Token": xAuthToken}
                };
                client.get("http://" + osIP + ":8774/v2/857a0f345123417c9f49d60847370476/os-quota-sets/" + req.body.projID + '/detail', args, function (data, response) {
                    // parsed response body as js object
                    getPqCreatedDate = new Date();
                    getPqCurrentDate = getPqCreatedDate;
                    res.send(data);
                });
            });
        }
        catch(err){
            res.send('');
        }
    }
    else {
        try {
            client.get("http://" + osIP + ":8774/v2/857a0f345123417c9f49d60847370476/os-quota-sets/" + req.body.projID + '/detail', args, function (data, response) {
                // parsed response body as js object
                res.send(data);
            });
        }
        catch(err){
            res.send('');
        }
    }
});

//app.use('/', routes);
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.sendFile(path.join(__dirname + '/public/404.html'));
        //res.render('error', {
        //  message: err.message,
        //  error: err
        //});
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.sendFile(path.join(__dirname + '/public/404.html'));
    //res.render('error', {
    //  message: err.message,
    //  error: {}
    //});
});


module.exports = app;