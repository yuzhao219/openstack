# COMPAS-OpenStack
A site to display live information about Stonybrook's OpenStack Cloud

### FRONT-END

* Uses Matarializecss for the front end. materializecss.com
* Uses the grid structure of materializecss. If you want to add another basic row, use the code below. If you want to have one row with more columns, check out the documentation at http://materializecss.com/grid.html
```html
        <div class="row">
            <div class="col s12">
            your content here
            </div>
        </row>
```

### BACK-END
* No database, just Node.js + Express
* To access the OpenStack API, you need keys. The keys last for a week, so when the server starts new keys are generated when the server starts and for every 7 days that the server is running.
* 
INDEX/ABOUT US
  -default page
  -
